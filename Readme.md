## TO UPDATE DOCKER IMAGE HASH JAR
```
mvn install:install-file -DgroupId=com.jars -DartifactId=dockerimagehash -Dversion=1.0.0 -Dfile=path to DockerImageHash.jar -Dpackaging=jar -DgeneratePom=true -DlocalRepositoryPath=path to maven-repo/repository -DcreateChecksum=true
```

## POM MODIFICATION
### ADD DEPENDENCY
```xml
</dependency>
    <dependency>
        <groupId>com.jars</groupId>
        <artifactId>dockerimagehash</artifactId>
        <version>1.0.0</version>
    </dependency>
</dependencies>
```

### ADD BITBUCKET REPO
```xml
<repositories>
    <repository>
        <id>repoid</id>
        <url>https://bitbucket.org/equatech/maven-repo/raw/master/repository/</url>
    </repository>
</repositories>
```